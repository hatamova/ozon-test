$(document).ready(function() {
    'use strict';
    var isOpen = false;
    var distributorBox = $('#distributor-box');

    visibleDistributorBox(isOpen);

    $('#distributor-link').on('click', function() {
        visibleDistributorBox(isOpen);
    });
    
    function visibleDistributorBox(status) {
		if ( status ) {
            distributorBox.show();
            $('#svg-arrow').css({'transform': 'rotate(90deg)'})
		}
		else {
            distributorBox.hide();
            $('#svg-arrow').css({'transform': 'rotate(-90deg)'})
        }
        isOpen = !isOpen;
	}
});